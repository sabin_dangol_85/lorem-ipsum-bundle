<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'knpuniversity/lorem-ipsum-bundle',
  ),
  'versions' => 
  array (
    'knpuniversity/lorem-ipsum-bundle' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.37',
      'version' => '4.4.37.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8c2d2c951ddedecb6d28954d336cb7d2e852d0e',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.37',
      'version' => '4.4.37.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de6c6e7f52b364840e53851c126be4d71e60470',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v4.4.39',
      'version' => '4.4.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d0fbcdb9317864b2bd9e49d570d88ae512cadf3',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.37',
      'version' => '4.4.37.0',
      'aliases' => 
      array (
      ),
      'reference' => '8d80ad881e1ce17979547873d093e3c987a6a629',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.37',
      'version' => '4.4.37.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ccfcfb96ecce1217d7b0875a0736976bc6e63dc',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.11',
      'version' => '1.1.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '01e9a4efac0ee33a05dfdf93b346f62e7d0e998c',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v5.4.6',
      'version' => '5.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd53a45039974952af7f7ebc461ccdd4295e29440',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec82e57b5b714dbb69300d348bd840b345e24166',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.4.6',
      'version' => '5.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '34e89bc147633c0f9dd6caaaf56da3b806a21465',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.39',
      'version' => '4.4.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '19d1cacefe81cb448227cc4d5909fb36e2e23081',
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v6.0.3',
      'version' => '6.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '81f5e8e453433e0182a49ca45d4734cb3a2f818f',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc5db0e22b3cb4111010e48785a97f670b350ca5',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4ba2d41b15f9bd0e19b2ab9674135813ec98f',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ab11b933cd6bc5464b08e81e2c5b07dec58b0fc',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.4.6',
      'version' => '5.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '294e9da6e2e0dd404e983daa5aa74253d92c05d0',
    ),
  ),
);
