<?php

namespace KnpU\LoremIpsumBundle\Event;

final class KnpULoremIpsumEvents
{
    /**
     * @Event("KnpU\LoremIpsumBundle\Event\FilterApiResponseEvent")
     */
    const FILTER_API = 'knpu_lorem_ipsum.filter_api';
}